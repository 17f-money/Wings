---
title: What is Wings?
subtitle: https://www.wings.ai/
date: 2017-11-09
---

Wings is a token on the Ethereum blockchain used for crowdfunding and predicting the success of crowdfunded projects.
There is about $44.5 million in Wings

<!--more-->

# Blockchain Information
  - The day the contract was made, the contract owner sent hundreds of transactions valued at either 0 or 0.001304225 Ether
  - The first transaction on the owner's address was 125 Ether received 4 hours before the creation of the Wings contract
  - The contract holds 0 Ether despite the official website showing hundreds of thousands of Wings used for crowdfunding
  - All the transactions sent to the contract contain 0 Ether  
  - The owner only holds 15 Wings